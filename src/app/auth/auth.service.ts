import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

import { HttpClient } from '@angular/common/http';
import { User } from './user';
import { throwError } from 'rxjs';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  AUTH_API: string = 'http://localhost:3000';

  constructor(private http: HttpClient) {}
  public getToken(): string {
    const token = localStorage.getItem('token');
    return token !== null ? token : '';
  }
  get isAuthenticated(): boolean {
    return true;
  }

  login(user: User) {
    return this.http
      .post<any>(`${this.AUTH_API}/login`, { mail: user.email, password: user.password })
      .pipe(take(1))
      .subscribe((res: any) => {
        localStorage.setItem('token', res.token);
      });
  }
  get isLoggedIn(): boolean {
    let authToken = localStorage.getItem('token');
    return authToken !== null ? true : false;
  }
  logout() {
    localStorage.removeItem('token');
  }
  handleError(error: HttpErrorResponse) {
    let msg = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      msg = error.error.message;
    } else {
      // server-side error
      msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(msg);
  }
}
