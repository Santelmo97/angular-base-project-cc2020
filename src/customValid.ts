import { AbstractControl, ValidatorFn } from '@angular/forms';

export function hasCapitalLetter(reg: RegExp): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    const forbidden = reg.test(control.value);
    return forbidden ? null : { forbiddenName: { value: control.value } };
  };
}
